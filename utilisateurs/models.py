from djongo import models

# Create your models here.

class Visiteur(models.Model):
    dateCreation = models.DateTimeField(auto_now_add=True)
    nbrVisite = models.BigIntegerField()
    derniereVisite = models.DateTimeField(auto_now_add=True)


class Role(models.Model):
    intitule = models.CharField(max_length=100)
    description = models.TextField()


class Employe(Visiteur, models.Model):
    nom = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    telephone = models.BigIntegerField

class Compte(models.Model):
    login = models.CharField(max_length=100)
    motDePasse = models.CharField(max_length=100)

    employe = models.ForeignKey(Employe, on_delete=models.CASCADE, null=True)
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING, null=True)