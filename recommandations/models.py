from djongo import models
from utilisateurs.models import Visiteur
from produits.models import Produit


class Consultation(models.Model):
    avis = models.IntegerField
    duree = models.DurationField

    visiteur = models.ForeignKey(Visiteur, on_delete=models.DO_NOTHING, null=True)
    produit = models.ForeignKey(Produit, on_delete=models.CASCADE, null=True)

class Recherche(models.Model):
    date = models.DateTimeField
    contenu = models.TextField

    visiteur = models.ForeignKey(Visiteur, on_delete=models.DO_NOTHING, null=True)
    produits = models.ManyToManyField(Produit)
