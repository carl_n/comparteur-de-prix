import sys, pprint, os
sys.path.insert(0, '../comparateurAPI')

from comparateurAPI.settings import AUTH_GMAIL
import imaplib, email
from email.policy import default


class BoiteMail:

    ATTACHMENT_DIR = "../comparateurAPI/acquisition/attachments"

    def __init__(self):
        self.con = imaplib.IMAP4_SSL(AUTH_GMAIL['SRV_IN_IMAP'])
        self.con.login(AUTH_GMAIL['USER_ADDRESS'], AUTH_GMAIL['PASSWORD'])

    def get_body(self, msg):
        if msg.is_multipart():
            return self.get_body(msg.get_payload(0))
        else:
            return msg.get_payload(None, True)

    def search(self, key, value):
        result, data = self.con.search(None, key, '"{}"'.format(value))
        return data

    def get_emails(self, result_bytes):
        msgs = []
        for num in result_bytes[0].split():
            typ, data = self.con.fetch(num, '(RFC822)')
            msgs.append(data)

        return msgs

    def get_inbox(self, emetteur):
        self.con.select('Inbox')

        msgs = self.get_emails(self.search('FROM', emetteur))

        for msg in msgs[::-1]:
            for sent in msg:
                if type(sent) is tuple:

                    content = str(sent[1], 'utf-8')
                    data = str(content)

                    try:
                        indexstart = data.find("ltr")
                        data2 = data[indexstart + 5: len(data)]
                        indexend = data2.find("</div>")

                        print(data2[0: indexend])

                    except UnicodeEncodeError as e:
                        pass

    def get_messages(self):
        with imaplib.IMAP4_SSL(AUTH_GMAIL['SRV_IN_IMAP']) as imap:
            r, d = imap.login(AUTH_GMAIL['USER_ADDRESS'], AUTH_GMAIL['PASSWORD'])
            imap.select("Inbox", readonly=True)
            r, d = imap.uid('search', None, "ALL")
            message_numbers = d[0].decode('utf8').split(' ')

            for msg_uid in reversed(message_numbers):
                r, d = imap.uid('FETCH', msg_uid, '(RFC822)')
                message = email.message_from_bytes(d[0][1], policy=default)

                if 'provenance' in message['subject']:
                    self.get_attachments(message)

                print("from:", message['from'])
                print("subject:", message['subject'])
                # Guess at "the" body part
                # Maybe parse this like before if it is an HTML part?
                # print(message.get_body().get_content())
                print()

    # allows you to download attachments
    def get_attachments(self, msg):
        print("Dans get_attachments")
        for part in msg.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            fileName = part.get_filename()

            if bool(fileName):
                print("Un fichier trouve")
                filePath = os.path.join(self.ATTACHMENT_DIR, fileName)
                with open(filePath, 'wb') as f:
                    f.write(part.get_payload(decode=True))


mailbox = BoiteMail()
mailbox.get_messages()
# mailbox.get_inbox("carl.nombo@gmail.com")

# connect to host using SSL
"""imap = imaplib.IMAP4_SSL(AUTH_GMAIL['SRV_IN_IMAP'])

## login to server
imap.login(AUTH_GMAIL['USER_ADDRESS'], AUTH_GMAIL['PASSWORD'])

imap.select('Inbox')

tmp, data = imap.search(None, 'ALL')
for num in data[0].decode('utf8').split(' '):
    tmp, data = imap.fetch(num, '(RFC822)')
    print('Message: {0}\n'.format(num))
    pprint.pprint(data[0][1])
    break
imap.close()"""
