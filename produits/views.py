from django.contrib.auth.models import User, Group
from rest_framework import permissions, status

from rest_framework.views import APIView
from rest_framework.response import Response

from produits.models import Categorie, Produit
from produits.serializers import CategorieSerializer, ProduitSerializer


class CategorieAPIView(APIView):

    def get(self, *args, **kwargs):
        categories = Categorie.objects.all();
        serializer = CategorieSerializer(categories, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CategorieSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProduitAPIView(APIView):

    def get(self, *args, **kwargs):
        produits = Produit.objects.all();
        serializer = ProduitSerializer(produits, many=True)
        return Response(serializer.data)