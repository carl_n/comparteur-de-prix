from django.contrib.auth.models import User, Group
from produits.models import Provenance, Categorie, HistAjout, Produit
from rest_framework import serializers


class CategorieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Categorie
        fields = ['id', 'label', 'description']


class ProduitSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'reference', 'description', 'unite', 'quantite', 'prix', 'marque',
                  'provenance', 'categorie', 'creation', 'mise_a_jour']